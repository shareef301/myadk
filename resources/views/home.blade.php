@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                @if (Session::has('success'))
                <div class="alert alert-success">
                  {{ Session::get('success') }}
                </div><br />
               @endif
                <a class="btn btn-outline-success btn-sm btnview" role="button" href="{{ url('synonyms/create') }}">NEW</a>
                <br>
                <br>
                <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Key</th>
                            <th scope="col">Tags</th>
                            <th></th>
                            <th></th>
                          </tr> 
                        </thead> 
                        <tbody>
                            @foreach ($Synonyms as $Synonym)
                                <tr>
                                    <th scope="row"></th>
                                    <td>{{$Synonym->key}}</td>
                                    <td>{{$Synonym->tags}}</td>
                                    <td><a class="btn btn-outline-success btn-sm btnview" role="button" href="{{ url('synonyms/'. $Synonym->id.'/edit') }}">EDIT</a></td>
                                    <td>
                                        <form action="{{action('SynonymsController@destroy', $Synonym->id)}}" method="post">
                                          @csrf
                                          <input name="_method" type="hidden" value="DELETE">
                                          <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>

                      <hr>
                      <h3>Pendding</h3>
                      <hr>

                      <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Key</th>
                            <th scope="col">Tags</th>
                            <th></th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($SynonymsNUll as $Synonym)
                                <tr>
                                    <th scope="row"></th>
                                    <td>{{$Synonym->key}}</td>
                                    <td>{{$Synonym->tags}}</td>
                                    <td><a class="btn btn-outline-success btn-sm btnview" role="button" href="{{ url('synonyms/'. $Synonym->id.'/edit') }}">EDIT</a></td>
                                    <td>
                                        <form action="{{action('SynonymsController@destroy', $Synonym->id)}}" method="post">
                                          @csrf
                                          <input name="_method" type="hidden" value="DELETE">
                                          <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>


        </div>
    </div>
</div>
@endsection
