@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

                <form action="{{ route('synonyms.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="key">Key</label>
                        <input type="text" class="form-control" name="key" id="key" aria-describedby="keyHelp" placeholder="Enter key name">
                        <small id="keyHelp" class="form-text text-muted">key name of the tags in next field.</small>
                    </div>
                    <div class="form-group">
                        <label for="tags">Tags</label>
                        <textarea type="text" class="form-control" name="tags" id="tags" placeholder="Tags"></textarea>
                        <small id="keyHelp" class="form-text text-muted">tags relating to the key name</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

        </div>
    </div>
</div> 
@endsection
