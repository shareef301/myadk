@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Edit</h2><br  />
      <form method="post" action="{{action('SynonymsController@update',$Synonyms->id )}}">
      @csrf
      <input name="_method" type="hidden" value="PATCH">
      <div class="row">
        <div class="col-md-4"></div>
        <div class="form-group col-md-4">
          <label for="ref">KEY:</label>
          <input type="text" class="form-control" name="key" id="key" value="{{$Synonyms->key}}">
        </div>
      </div>
      <div class="row">
        <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="amount">TAGS:</label>
            <input type="text" class="form-control" name="tags" id="tags" value="{{$Synonyms->tags}}">
          </div>
        </div> 
       

      <div class="row">
        <div class="col-md-4"></div>
        <div class="form-group col-md-4" style="margin-top:60px">
          <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
        </div>
      </div>
    </form>
  </div>

  @endsection