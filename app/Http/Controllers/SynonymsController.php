<?php

namespace App\Http\Controllers;

use App\Synonyms;
use Illuminate\Http\Request;

class SynonymsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'key' => 'required',
            'tags' => 'required',
        ]);

        Synonyms::create($request->all());

        return redirect()->route('home')->with('success','Synonyms added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Synonyms  $synonyms
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        
        $Synonyms = Synonyms::find($id);

        // dd($voucher->bill);
        if(empty($Synonyms)){
            abort(404);
        }
        return view('show')->with('synonyms',$Synonyms);
    //     $id = request()->segment(count(request()->segments()));
    //     $Synonyms=Synonyms::Where('id','=',unserialize($id) );
        
    //     return view('show')->with('synonyms',$Synonyms);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Synonyms  $synonyms
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Synonyms = Synonyms::find($id);
        return view('edit')->with(compact('Synonyms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Synonyms  $synonyms
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $Synonyms = Synonyms::find($id);
        $key = $request->key;
        $tags = $request->tags;
        $Synonyms->key = $key;
        $Synonyms->tags = $tags;
    

        if( $Synonyms->save() === true ){
            return redirect('home')->with('success', 'Information has been added');
        }
        else{
            return redirect('home')->with('error', 'could not add');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Synonyms  $synonyms
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $synonyms = Synonyms::find($id);
        $synonyms->delete();
        return redirect('home')->with('success','Information has been  deleted');
    }
}
