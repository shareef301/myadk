<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Synonyms;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Synonyms=Synonyms::whereNotNull('tags')->get();

        $SynonymsNUll=Synonyms::whereNull('tags')->get();

        return view('home')->with(compact('Synonyms','SynonymsNUll'));
    }
}
