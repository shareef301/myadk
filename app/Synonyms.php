<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Synonyms extends Model
{
    protected $fillable = ['key','tags'];
}
