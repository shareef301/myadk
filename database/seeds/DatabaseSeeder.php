<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert(array(
            array('name'=>'admin','email'=>'admin@example.com','password'=>'$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm'),
        ));
 
        DB::table('synonyms')->insert(array(
            array('key'=>'niyaf','tags'=>'niaf,niyaaf,niaaf,niaph,niyaaph'),
            array('key'=>'yogita', 'tags'=>'yogeetha, yoqita, yogheetha, yogheeta, yogeeta, yoqita, yoqeeta, yoqeetha, yoqithaa'),
        ));
    }
}
